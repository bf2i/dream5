import pandas as pd
import numpy as np
from dream5.configuration import grn_folder
from dream5.id_convertion import G2symbol_saureus
from dream5.grn import load_grn_saureus


def generate_saureus_gold_std_GRN(file_name, path):
    """
    Creates a file with the full gold standard of S. aureus (including negative interactions)
    Useful for the evaluation of an estimated GRN for S. aureus
    The gold standard given in the DREAM5 dataset doesn't include negative interactions

    Args:
        file_name: output file with full S. aureus GRN gold standard
        path: path to save the GRN file

    Returns:
        pandas.DataFrame: full GRN gold standard for S. aureus

    """

    ### adds interactions with IS_REGULATED = 0 (not included for S. aureus in given gold standard)
    grn = load_grn_saureus()
    grn = grn[grn["TF"] != grn["TG"]]
    tfs = list(np.unique(grn["TF"]))
    tgs = list(np.unique(grn["TG"]))
    links = []

    for tf in tfs:
        for tg in np.unique(tfs+tgs):
            if tf != tg:
                links.append([tf, tg, 0])

    links = pd.DataFrame(links, columns = ["TF","TG","IS_REGULATED"])
    links.index = links["TF"] + "_" + links["TG"]
    links.loc[grn.index,"IS_REGULATED"] = 1

    symbol2G = {s: g for g, s in G2symbol_saureus.items()} # inverting dictionary
    links["TF"] = links["TF"].map(symbol2G)
    links["TG"] = links["TG"].map(symbol2G)
    links.to_csv(path+file_name,sep="\t",header=None, index=None)

    return links


if __name__ == '__main__':
    # path = "/Users/pschmidtt/Documents/databases/dream/DREAM5/test_data"
    path = "/Users/pschmidtt/Documents/databases/dream/"
    file_name = "DREAM5_NetworkInference_GoldStandard_Network2-saureus_NEW.txt"
    print(generate_saureus_gold_std_GRN(file_name, path).head())
