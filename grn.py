"""
Load gene regulatory networks
------------------

Loads dream5 gene regulatory networks

Example:
    Test the example by running this file:

        $ python grn.py
"""

import pandas as pd
import numpy as np
from os.path import join
from dream5.configuration import grn_folder
from dream5.id_convertion import G2symbol_insilico
from dream5.id_convertion import G2symbol_saureus
from dream5.id_convertion import G2symbol_ecoli
from dream5.id_convertion import G2symbol_scerevisiae


__author__ = "Pauline Schmitt"
__copyright__ = "Copyright 2019, The Dream5 Project"
__credits__ = ["Sergio Peignier"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Pauline Schmitt"
__email__ = "pauline.schmitt@insa-lyon.fr"
__status__ = "pre-alpha"

def load_grn_insilico():
    """
    Load dream5 in silico GRN

    Gene expression profiles derived from GeneNetWeaver[1]
    Contains 4012 interactions with strong evidence labelled as '1'
    Contains 178 TFs and 1387 TGs that are not TFs (1498 TGs in total)
    Remaining possible connections (178*(1387+178)-178) starting from the 178 TFs are labelled as '0' (self loops aren't considered)

    [1] D. Marbach, T. Schaffter, C. Mattiussi, and D. Floreano. Generating realistic in silico gene networks for performance assessment of reverse engineering methods. Journal of Computational Biology, 16(2):229–239, 2009.

    Returns:
        pandas.DataFrame: grn
        column "TF" represent the regulators and "TG" the target genes
        colmun IS_REGULATED indicates whether the TF regulates the TG (1) or not (0)

    Examples:
        >>> my_grn = load_grn_insilico()
        >>> my_grn.head()

                     TF    TG  IS_REGULATED
        crp_glpR    crp  glpR             1
        cytR_glpR  cytR  glpR             1
        fnr_tdcA    fnr  tdcA             1
        crp_tdcA    crp  tdcA             1
        ihfA_tdcA  ihfA  tdcA             1

    """
    grn = pd.read_csv(join(grn_folder, "DREAM5_NetworkInference_GoldStandard_Network1-in-silico.tsv"),sep="\t",header=None)
    grn.columns = ['TF', 'TG', 'IS_REGULATED']
    # change name Gx to symbol of genes
    grn["TF"] = grn["TF"].map(G2symbol_insilico)
    grn["TG"] = grn["TG"].map(G2symbol_insilico)
    grn.index = grn["TF"]+"_"+grn["TG"]
    return (grn.drop_duplicates())

def load_grn_saureus():
    """
    Load dream5 saureus GRN

    Evaluated using RegPrecise, as there currently does not exist a sufficiently large set of experimentally validated interactions

    Returns:
        pandas.DataFrame: grn
        column "TF" represent the regulators and "TG" the target genes
        colmun IS_REGULATED indicates whether the TF regulates the TG (1) or not (0)

    Examples:
        >>> my_grn = load_grn_saureus()
        >>> my_grn.head()

                              TF       TG  IS_REGULATED
        SAV2046_SAV0605  SAV2046  SAV0605             1
        SAV2046_SAV0148  SAV2046  SAV0148             1
        SAV2046_SAV2635  SAV2046  SAV2635             1
        SAV2046_SAV2634  SAV2046  SAV2634             1
        SAV2046_SAV2633  SAV2046  SAV2633             1

    """
    grn = pd.read_csv(join(grn_folder, "DREAM5_NetworkInference_GoldStandard_Network2-saureus.txt"),sep="\t",header=None)
    grn.columns = ['TF', 'TG', 'IS_REGULATED']
    # change name Gx to symbol of genes
    grn["TF"] = grn["TF"].map(G2symbol_saureus)
    grn["TG"] = grn["TG"].map(G2symbol_saureus)
    grn.index = grn["TF"]+"_"+grn["TG"]
    # use function generate_saureus_gold_std_GRN from grn_saureus if full GRN is required (including IS_REGULATED = 0 interactions)

    return (grn.drop_duplicates())

def load_grn_ecoli():
    """
    Load dream5 ecoli GRN

    Gold Standard built from RegulonDB Release 6.8.
    Contains 2066 interactions with strong evidence labelled as '1'
    Contains 141 TFs and 940 TGs that are not TFs (999 TGs in total)
    Remaining possible connections (141*(940+141)-141) starting from the 141 TFs are labelled as '0' (self loops aren't considered)
    Interactions labelled as '1' have strong evidence
    Others could be real interactions that haven't been experimentally found yet

    Returns:
        pandas.DataFrame: grn
        column "TF" represent the regulators and "TG" the target genes
        colmun IS_REGULATED indicates whether the TF regulates the TG (1) or not (0)

    Examples:
        >>> my_grn = load_grn_ecoli()
        >>> my_grn.head()

                     TF    TG  IS_REGULATED
        fnr_nikR    fnr  nikR             1
        argP_dnaA  argP  dnaA             1
        crp_tdcA    crp  tdcA             1
        tdcR_tdcA  tdcR  tdcA             1
        ihfB_tdcA  ihfB  tdcA             1


    """
    grn = pd.read_csv(join(grn_folder, "DREAM5_NetworkInference_GoldStandard_Network3-ecoli.tsv"),sep="\t",header=None)
    grn.columns = ['TF', 'TG', 'IS_REGULATED']
    # change name Gx to symbol of genes
    grn["TF"] = grn["TF"].map(G2symbol_ecoli)
    grn["TG"] = grn["TG"].map(G2symbol_ecoli)
    grn.index = grn["TF"]+"_"+grn["TG"]
    return (grn.drop_duplicates())

def load_grn_scerevisiae():
    """
    Load dream5 scerevisiae GRN

    Gold Standard built from [1] based on ChIP-chip data TFBS motifs analysis
    Contains 3940 interactions with strong evidence labelled as '1'
    Contains 114 TFs and 1880 TGs that are not TFs (1934 TGs in total)
    Remaining possible connections (114*(1880+114)-114) starting from the 114 TFs are labelled as '0' (self loops aren't considered)
    Interactions labelled as '1' have strong evidence
    Others could be real interactions that haven't been experimentally found yet`

    [1] K. D. MacIsaac, T. Wang, D. B. Gordon, D. K. Gifford, G. D. Stormo, and E. Fraenkel. An improved map of conserved regulatory sites for saccharomyces cerevisiae. BMC Bioinformatics, 7:113, 2006.

    Returns:
        pandas.DataFrame: grn
        column "TF" represent the regulators and "TG" the target genes
        colmun IS_REGULATED indicates whether the TF regulates the TG (1) or not (0)

    Examples:
        >>> my_grn = load_grn_scerevisiae()
        >>> my_grn.head()

                              TF       TG  IS_REGULATED
        YKL043W_YHR084W  YKL043W  YHR084W             1
        YPL049C_YHR084W  YPL049C  YHR084W             1
        YIL131C_YHR084W  YIL131C  YHR084W             1
        YDR043C_YBR150C  YDR043C  YBR150C             1
        YPL202C_YGL071W  YPL202C  YGL071W             1

    """
    grn = pd.read_csv(join(grn_folder, "DREAM5_NetworkInference_GoldStandard_Network4-scerevisiae.tsv"),sep="\t",header=None)
    grn.columns = ['TF', 'TG', 'IS_REGULATED']
    # change name Gx to symbol of genes
    grn["TF"] = grn["TF"].map(G2symbol_scerevisiae)
    grn["TG"] = grn["TG"].map(G2symbol_scerevisiae)
    grn.index = grn["TF"]+"_"+grn["TG"]
    return (grn.drop_duplicates())

if __name__ == '__main__':
    print("Loading gene regulatory networks")
    grn_insilico = load_grn_insilico()
    grn_ecoli = load_grn_ecoli()
    grn_saureus = load_grn_saureus()
    grn_scerevisiae = load_grn_scerevisiae()
    print("\n--- in silico data ---")
    print(grn_insilico.head())
    print("     size : ", grn_insilico.shape)
    print("\n--- saureus data ---")
    print(grn_saureus.head())
    print("     size : ", grn_saureus.shape)
    print("\n--- ecoli data ---")
    print(grn_ecoli.head())
    print("     size : ", grn_ecoli.shape)
    print("\n--- scerevisiae data ---")
    print(grn_scerevisiae.head())
    print("     size : ", grn_scerevisiae.shape)
