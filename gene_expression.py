"""
Load gene expression datasets
-----------------------------

Loads gene expression datasets

Example:
    Test the example by running this file::

        $ python gene_expression.py
"""

__author__ = "Pauline Schmitt"
__copyright__ = "Copyright 2019, The Dream5 Project"
__credits__ = ["Sergio Peignier"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Pauline Schmitt"
__email__ = "pauline.schmitt@insa-lyon.fr"
__status__ = "pre-alpha"

from os.path import join
from pandas import read_csv
from dream5.configuration import library_folder
from dream5.id_convertion import G2symbol_insilico
from dream5.id_convertion import G2symbol_saureus
from dream5.id_convertion import G2symbol_ecoli
from dream5.id_convertion import G2symbol_scerevisiae

def load_insilico():
    """
    Load in silico dataset

    Returns:
        pandas.DataFrame: dream5 in silico gene expression dataset
        rows represent genes and columns represent conditions

    Examples:
        >>> df_insilico = load_insilico()
        >>> df_insilico.head()

                0       ...     804
        tyrR  0.425448  ...  0.681207
        glpR  0.017829  ...  0.000560
        tdcA  0.907989  ...  0.373693
        allS  0.448247  ...  0.719729
        mtlR  0.172324  ...  0.070691
    """
    expr_data = read_csv(join(library_folder, "network1-in-silico/net1_expression_data.tsv"),sep="\t",header=0)
    expr_data = expr_data.transpose()
    # replace row index with corresponding value in dictionary
    new_index = expr_data.index.map(G2symbol_insilico)
    expr_data.index = new_index
    return expr_data

def load_saureus():
    """
    Load in saureus dataset

    Returns:
        pandas.DataFrame: dream5 saureus gene expression dataset
        rows represent genes and columns represent conditions

    Examples:
        >>> df_saureus = load_saureus()
        >>> df_saureus.head()

                       0        ...     159
        decoy5     11.4170      ...     11.3790
        SACOL2524   3.4503      ...     6.2626
        SAV1564     8.7697      ...     8.8521
        SAV0535    10.2360      ...     11.2140
        SAV1561    10.6820      ...     9.5807
    """
    expr_data = read_csv(join(library_folder, "network2-saureus/net2_expression_data.tsv"),sep="\t",header=0)
    expr_data = expr_data.transpose()
    # replace row index with corresponding value in dictionary
    new_index = expr_data.index.map(G2symbol_saureus)
    expr_data.index = new_index
    return expr_data

def load_ecoli():
    """
    Load in ecoli dataset

    Returns:
        pandas.DataFrame: dream5 ecoli gene expression dataset
        rows represent genes and columns represent conditions

    Examples:
        >>> df_ecoli = load_ecoli()
        >>> df_ecoli.head()

                0     ...  804
        yneL  7.1151  ...  6.8575
        nikR  9.3293  ...  8.1178
        dnaA  9.5997  ...  11.2740
        tdcA  6.9998  ...  6.5021
        yhjB  7.4955  ...  7.5756
    """
    expr_data = read_csv(join(library_folder, "network3-ecoli/net3_expression_data.tsv"),sep="\t",header=0)
    expr_data = expr_data.transpose()
    # replace row index with corresponding value in dictionary
    new_index = expr_data.index.map(G2symbol_ecoli)
    expr_data.index = new_index
    return expr_data

def load_scerevisiae():
    """
    Load in scerevisiae dataset

    Returns:
        pandas.DataFrame: dream5 scerevisiae gene expression dataset
        rows represent genes and columns represent conditions

    Examples:
        >>> df_scerevisiae = load_scerevisiae()
        >>> df_scerevisiae.head()

                    0       ...      535
        YHR084W   8.3066    ...  10.3480
        YER109C   9.7335    ...   8.4520
        YDR496C  10.3450    ...  10.4200
        YBR098W   9.1199    ...   8.1838
        YNL139C  10.3480    ...   9.3826
    """
    expr_data = read_csv(join(library_folder, "network4-scerevisiae/net4_expression_data.tsv"),sep="\t",header=0)
    expr_data = expr_data.transpose()
    # replace row index with corresponding value in dictionary
    new_index = expr_data.index.map(G2symbol_scerevisiae)
    expr_data.index = new_index
    return expr_data

if __name__ == '__main__':
    print("Loading gene expression data")
    print("\n--- in silico data ---")
    print(load_insilico().head())
    print("\n--- saureus data ---")
    print(load_saureus().head())
    print("\n--- ecoli data ---")
    print(load_ecoli().head())
    print("\n--- scerevisiae data ---")
    print(load_scerevisiae().head())
