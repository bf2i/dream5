from scipy.io import loadmat
from os.path import join
import pandas as pd
from dream5.configuration import auc_folder
from dream5.configuration import p_values_folder
from grn.grn_evaluation import fit_beta_pdf


def load_auc_insilico ():
    """
    Load AUPR and AUROC distribution for in silico random GRNs

    Returns:
        Xpr: (numpy.ndarray) x values of the AUPR distribution
        Ypr: (numpy.ndarray) y values of the AUPR distribution
        Xroc: (numpy.ndarray) x values of the AUROC distribution
        Yroc: (numpy.ndarray) y values of the AUROC distribution
    """
    AUPR = loadmat(join(auc_folder, 'Network1_AUPR.mat'))
    AUROC = loadmat(join(auc_folder, 'Network1_AUROC.mat'))
    Xpr = AUPR["X"][0]
    Ypr = AUPR["Y"][0]
    Xroc = AUROC["X"][0]
    Yroc = AUROC["Y"][0]
    return Xpr, Ypr, Xroc, Yroc

def load_auc_ecoli ():
    """
    Load AUPR and AUROC distribution for E. coli random GRNs

    Returns:
        Xpr: (numpy.ndarray) x values of the AUPR distribution
        Ypr: (numpy.ndarray) y values of the AUPR distribution
        Xroc: (numpy.ndarray) x values of the AUROC distribution
        Yroc: (numpy.ndarray) y values of the AUROC distribution
    """
    AUPR = loadmat(join(auc_folder, 'Network3_AUPR.mat'))
    AUROC = loadmat(join(auc_folder, 'Network3_AUROC.mat'))
    Xpr = AUPR["X"][0]
    Ypr = AUPR["Y"][0]
    Xroc = AUROC["X"][0]
    Yroc = AUROC["Y"][0]
    return Xpr, Ypr, Xroc, Yroc

def load_auc_scerevisiae ():
    """
    Load AUPR and AUROC distribution for S. cerevisiae random GRNs

    Returns:
        Xpr: (numpy.ndarray) x values of the AUPR distribution
        Ypr: (numpy.ndarray) y values of the AUPR distribution
        Xroc: (numpy.ndarray) x values of the AUROC distribution
        Yroc: (numpy.ndarray) y values of the AUROC distribution
    """
    AUPR = loadmat(join(auc_folder, 'Network4_AUPR.mat'))
    AUROC = loadmat(join(auc_folder, 'Network4_AUROC.mat'))
    Xpr = AUPR["X"][0]
    Ypr = AUPR["Y"][0]
    Xroc = AUROC["X"][0]
    Yroc = AUROC["Y"][0]
    return Xpr, Ypr, Xroc, Yroc

def load_auc_saureus():
    """
    Load AUPR and AUROC distribution for S. aureus random GRNs
    To use this function you need the file saureus.csv, containing scores of the randomly generated GRNs for S. aureus, that are not given with the DREAM5 dataset
    You can generate the file saureus.csv with the function generate_rand_metrics() of this package

    Returns:
        Xpr: (numpy.ndarray) x values of the AUPR distribution
        Ypr: (numpy.ndarray) y values of the AUPR distribution
        Xroc: (numpy.ndarray) x values of the AUROC distribution
        Yroc: (numpy.ndarray) y values of the AUROC distribution
    """
    saureus_rand = pd.read_csv(join(p_values_folder, "saureus.csv"), index_col=0)
    auroc = saureus_rand["AUROC"].values
    aupr = saureus_rand["AUPR"].values
    Xroc, Yroc = fit_beta_pdf(auroc)
    Xpr, Ypr = fit_beta_pdf(aupr)
    return Xpr, Ypr, Xroc, Yroc
