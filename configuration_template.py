"""
Configuration file
------------------
Configuration file, contains some paths and constants that are used by other
modules from the dream5 project

Attributes:
    library_folder (str): Path to the databases folder
    id_folder (str) : Path to the ids dictionary folder
    grn_folder (str) : path to gene regulatory networks (test results) folder
"""

__author__ = "Pauline Schmitt"
__copyright__ = "Copyright 2019, The Dream5 Project"
__credits__ = ["Sergio Peignier"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Pauline Schmitt"
__email__ = "pauline.schmitt@insa-lyon.fr"
__status__ = "pre-alpha"

library_folder = "/my/path/databases/dream/DREAM5/train_data"
id_folder = "/my/path/databases/dream/DREAM5/train_data/ids"
grn_folder = "/my/path/databases/dream/DREAM5/test_data"
