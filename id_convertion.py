"""
Configuration file
------------------
Configuration file, contains some paths and constants that are used by other
modules from the dream5 project

Attributes:
    G2symbol_scerevisiae (dict) : convertion dictionnary G2symbol for scerevisiae data
    G2symbol_ecoli (dict) : convertion dictionnary G2symbol for ecoli data
    G2symbol_saureus (dict) : convertion dictionnary G2symbol for saureus data
    G2symbol_in-silico (dict) : convertion dictionnary G2symbol for in silico data
    id2locus_scerevisiae (dict) : convertion dictionnary from Uniprot ID to locus tag for scerevisiae data
    id2locus_saureus (dict) : convertion dictionnary from Uniprot ID to locus tag for saureus data
    id2locus_ecoli (dict) : convertion dictionnary from Uniprot ID to locus tag for ecoli data
    id2name_ecoli (dict) convertion dictionnary from Uniprot ID to gene names (gene symbol) for ecoli data
    locus2id_saureus (dict) : convertion dictionnary from locus tag to Uniprot ID for saureus data.
    locus2id_scerevisiae (dict) : convertion dictionnary from locus tag to Uniprot ID for scerevisiae data.
    name2id_ecoli (dict) : convertion dictionnary from gene symbol to Uiprot ID for ecoli data.
Example:
    Test the example by running this file::

        $ python id_convertion.py

Todo:
"""

__author__ = "Pauline Schmitt, Baptiste Sorin"
__copyright__ = "Copyright 2019, The Dream5 Project"
__credits__ = ["Sergio Peignier"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Pauline Schmitt"
__email__ = "pauline.schmitt@insa-lyon.fr"
__status__ = "pre-alpha"

from dream5.configuration import library_folder
from dream5.configuration import id_folder
from pickle import load
from os.path import join


# Load IDs dictionnaries
G2symbol_scerevisiae = load(open(join(id_folder,"G2symbol_scerevisiae.pickle"),"rb"))
G2symbol_ecoli = load(open(join(id_folder,"G2symbol_ecoli.pickle"),"rb"))
G2symbol_saureus = load(open(join(id_folder,"G2symbol_saureus.pickle"),"rb"))
G2symbol_insilico = load(open(join(id_folder,"G2symbol_in-silico.pickle"),"rb"))

id2locus_saureus = load(open(join(id_folder,"id_to_locus_saureus.pickle"),"rb"))
id2locus_scerevisiae = load(open(join(id_folder,"id_to_locus_scerevisiae.pickle"),"rb"))
id2locus_ecoli = load(open(join(id_folder,"id_to_locus_ecoli.pickle"),"rb"))

id2name_ecoli = load(open(join(id_folder,"id_to_name_ecoli.pickle"),"rb"))
name2id_ecoli = load(open(join(id_folder,"name_to_id_ecoli.pickle"),"rb"))
id2name_all= load(open(join(id_folder,"id_to_name_all.pickle"),"rb"))

locus2id_saureus = load(open(join(id_folder,"locus_to_id_saureus.pickle"),"rb"))
locus2id_scerevisiae = load(open(join(id_folder,"locus_to_id_scerevisiae.pickle"),"rb"))
if __name__ == '__main__':
    print("Loading dictionaries of symbols and names for genes")
    print("\n--- scerevisiae data ---")
    print(G2symbol_scerevisiae)
    print("\n--- ecoli data ---")
    print(G2symbol_ecoli)
    print("\n--- saureus data ---")
    print(G2symbol_saureus)
    print("\n--- in silico data ---")
    print(G2symbol_insilico)
