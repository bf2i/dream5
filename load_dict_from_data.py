__author__ = "Pauline Schmitt"
__copyright__ = "Copyright 2019, The Dream5 Project"
__credits__ = ["Sergio Peignier"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Pauline Schmitt"
__email__ = "pauline.schmitt@insa-lyon.fr"
__status__ = "pre-alpha"

"""
The tsv files mentionned below can be found on the uniprot database 
S.cerevisiae : https://www.uniprot.org/uniprot/?query=yourlist:M202007215C475328CEF75220C360D524E9D456CE29DFBDG&fil=organism%3A%22Saccharomyces+cerevisiae+%28strain+ATCC+204508+%2F+S288c%29+%28Baker%27s+yeast%29+%5B559292%5D%22&columns=yourlist(M202007215C475328CEF75220C360D524E9D456CE29DFBDG),id,genes(OLN),organism&sort=yourlist:M202007215C475328CEF75220C360D524E9D456CE29DFBDG
S.aureus : https://www.uniprot.org/uniprot/?query=yourlist:M202007215C475328CEF75220C360D524E9D456CE29DFF27&sort=yourlist:M202007215C475328CEF75220C360D524E9D456CE29DFF27&columns=yourlist(M202007215C475328CEF75220C360D524E9D456CE29DFF27),id,genes(OLN),organism
E.coli : https://www.uniprot.org/uniprot/?query=yourlist:M202007215C475328CEF75220C360D524E9D456CE29E0DD0&sort=yourlist:M202007215C475328CEF75220C360D524E9D456CE29E0DD0&columns=yourlist(M202007215C475328CEF75220C360D524E9D456CE29E0DD0),id,genes(OLN),organism
"""

from pandas import read_csv
from pickle import dump
from os.path import join
from dream5.configuration import library_folder
from dream5.configuration import id_folder

def to_locus_tag(row):
    if type_serie.at[row]==float:
        ls=cerevisiae_tab.at[row,"Gene names"].split(' ')
        return(ls[0])
    else:
        return(cerevisiae_tab.at[row,"Gene names  (ordered locus )"])
def to_gene_name(ls):
    ls=ls.split(' ')
    return(ls[0])

#for scerevisiae
id_df_sc = read_csv(join(library_folder, "network4-scerevisiae/net4_gene_ids.tsv"),sep="\t",header=0, index_col=0)
id_dict_sc = id_df_sc.to_dict()["Name"]
dump(id_dict_sc, open(join(id_folder, "G2symbol_scerevisiae.pickle"), "wb"))
cerevisiae_tab=read_csv(join(library_folder,"tsv_files/cerevisiae.tab",sep='\t',index_col=0) 
type_serie = cerevisiae_tab["Gene names  (ordered locus )"].apply(type)
cerevisiae_tab["Gene name"]=pd.Series(cerevisiae_tab.index).apply(to_locus_tag).values
cerevisiae_tab1=pd.DataFrame(data=cerevisiae_tab.index,index=cerevisiae_tab["Gene name"])
dump(list(cerevisiae_tab1.to_dict().values())[0],
     open(join(id_folder,'locus_to_id_scerevisiae.pickle'), 'wb'))
cerevisiae_tab=pd.DataFrame(data=cerevisiae_tab["Gene name"],index=cerevisiae_tab.index)
dump(list(cerevisiae_tab1.to_dict().values())[0],
     open(join(id_folder,'id_to_locus_scerevisiae.pickle'), 'wb'))
# ? G2symbol_scerevisiae = load(open(join(library_folder, "ids/G2symbol_scerevisiae.pickle"), "rb"))

#for ecoli
id_df_ec = read_csv(join(library_folder, "network3-ecoli/net3_gene_ids.tsv"),sep="\t",header=0, index_col=0)
id_dict_ec = id_df_ec.to_dict()["Name"]
dump(id_dict_ec, open(join(id_folder, "G2symbol_ecoli.pickle"), "wb"))
coli_tab=read_csv(join(library_folder,"tsv_files/tsv_ecoli.tab",sep='\t',index_col=0)
coli_tab["Gene names"]=coli_tab["Gene names"].apply(to_gene_name)
coli_tab1=pd.DataFrame(data=coli_tab.index,index=coli_tab['Gene names'])
dict_coli=list(coli_tab1.to_dict().values())[0]
dump(dict_coli, open(join(id_folder,'name_to_id_ecoli.pickle'), 'wb'))
coli_tab=pd.DataFrame(data=coli_tab['Gene names'],index=coli_tab.index)
dict_coli=list(coli_tab.to_dict().values())[0]
dump(dict_coli, open(join(id_folder,'id_to_name_ecoli.pickle'), 'wb'))

#for saureus
id_df_sa = read_csv(join(library_folder, "network2-saureus/net2_gene_ids.tsv"),sep="\t",header=0, index_col=0)
id_dict_sa = id_df_sa.to_dict()["Name"]
dump(id_dict_sa, open(join(id_folder, "G2symbol_saureus.pickle"), "wb"))
aureus_tab = read_csv(join(library_folder,"tsv_files/tsv_aureus.tab",sep='\t',index_col=0)
aureus_tab1=pd.DataFrame(data=aureus_tab["Entry"],index=aureus_tab.index)
dump(list(aureus_tab1.to_dict().values())[0],
open(join(id_folder,'locus_to_id_saureus.pickle'), 'wb'))
aureus_tab=pd.DataFrame(data=aureus_tab.index,index=aureus_tab["Entry"])
dump(list(aureus_tab.to_dict().values())[0],
     open(join(id_folder,'id_to_locus_saureus.pickle'), 'wb'))
            
#in silico
id_df_ins = read_csv(join(library_folder, "network1-in-silico/net1_gene_ids.tsv"),sep="\t",header=0, index_col=0)
id_dict_ins = id_df_ins.to_dict()["Name"]
dump(id_dict_ins, open(join(id_folder, "G2symbol_in-silico.pickle"), "wb"))

dic_ttl ={**list(aureus_tab.to_dict().values())[0],**dict_coli,**list(cerevisiae_tab1.to_dict().values())[0]}

dump(dic_ttl,open(join(id_folder,'id_to_locus_all.pickle'), 'wb'))
            

            
            





