"""
dream5 TF datasets
------------------

This module allows to load lists of Transcription Factors for the dream5 dataset
To use this library you should also have the related datasets.

Example:
    Test the example by running this file::

        $ python tf.py

Todo:

"""

from os.path import join
from pandas import read_csv
from dream5.configuration import library_folder
from dream5.id_convertion import G2symbol_insilico
from dream5.id_convertion import G2symbol_saureus
from dream5.id_convertion import G2symbol_ecoli
from dream5.id_convertion import G2symbol_scerevisiae


__author__ = "Pauline Schmitt"
__copyright__ = "Copyright 2019, The Dream5 Project"
__credits__ = ["Sergio Peignier"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Pauline Schmitt"
__email__ = "pauline.schmitt@insa-lyon.fr"
__status__ = "pre-alpha"


def load_tf_insilico():
    """
    Load in silico transcription factors

    Returns:
        pandas.DataFrame: dataframe with dream5 in silico transcription factors
        rows represent genes
        1st colmun represents codes (Gx ; x a number), 2nd colmun represents symbols

    Examples:
        >>> tf_insilico = load_tf_insilico()
        >>> tf_insilico.head()

            0     1
        0  G1  tyrR
        1  G2  glpR
        2  G3  tdcA
        3  G4  allS
        4  G5  mtlR
    """
    # load the list of transcription factors into a dataframe (format G1, G2, ...)
    tf_data = read_csv(join(library_folder, "network1-in-silico/net1_transcription_factors.tsv"),sep="\t",header=None)
    # add a column with symbol names of the genes, from dictionary
    tf_data[1] = tf_data[0].map(G2symbol_insilico)
    return (tf_data)

def load_tf_saureus():
    """
    Load saureus transcription factors

    Returns:
        pandas.DataFrame: dataframe with dream5 saureus transcription factors
        rows represent genes
        1st colmun represents codes (Gx ; x a number), 2nd colmun represents symbols

    Examples:
        >>> tf_saureus = load_tf_saureus()
        >>> tf_saureus.head()

            0          1
        0  G1     decoy5
        1  G2  SACOL2524
        2  G3    SAV1564
        3  G4    SAV0535
        4  G5    SAV1561
    """
    # load the list of transcription factors into a dataframe (format G1, G2, ...)
    tf_data = read_csv(join(library_folder, "network2-saureus/net2_transcription_factors.tsv"),sep="\t",header=None)
    # add a column with symbol names of the genes, from dictionary
    tf_data[1] = tf_data[0].map(G2symbol_saureus)
    return (tf_data)

def load_tf_ecoli():
    """
    Load ecoli transcription factors

    Returns:
        pandas.DataFrame: dataframe with dream5 ecoli transcription factors
        rows represent genes
        1st colmun represents codes (Gx ; x a number), 2nd colmun represents symbols

    Examples:
        >>> tf_ecoli = load_tf_ecoli()
        >>> tf_ecoli.head()

            0     1
        0  G1  yneL
        1  G2  nikR
        2  G3  dnaA
        3  G4  tdcA
        4  G5  yhjB
    """
    # load the list of transcription factors into a dataframe (format G1, G2, ...)
    tf_data = read_csv(join(library_folder, "network3-ecoli/net3_transcription_factors.tsv"),sep="\t",header=None)
    # add a column with symbol names of the genes, from dictionary
    tf_data[1] = tf_data[0].map(G2symbol_ecoli)
    return (tf_data)

def load_tf_scerevisiae():
    """
    Load scerevisiae transcription factors

    Returns:
        pandas.DataFrame: dataframe with dream5 scerevisiae transcription factors
        rows represent genes
        1st colmun represents codes (Gx ; x a number), 2nd colmun represents symbols

    Examples:
        >>> tf_scerevisiae = load_tf_scerevisiae()
        >>> tf_scerevisiae.head()

            0        1
        0  G1  YHR084W
        1  G2  YER109C
        2  G3  YDR496C
        3  G4  YBR098W
        4  G5  YNL139C
    """
    # load the list of transcription factors into a dataframe (format G1, G2, ...)
    tf_data = read_csv(join(library_folder, "network4-scerevisiae/net4_transcription_factors.tsv"),sep="\t",header=None)
    # add a column with symbol names of the genes, from dictionary
    tf_data[1] = tf_data[0].map(G2symbol_scerevisiae)
    return (tf_data)

if __name__ == '__main__':
    print("Loading transcription factors list (name and symbol)")
    print("\n--- in silico data ---")
    print(load_tf_insilico().head())
    print("\n--- saureus data ---")
    print(load_tf_saureus().head())
    print("\n--- ecoli data ---")
    print(load_tf_ecoli().head())
    print("\n--- scerevisiae data ---")
    print(load_tf_scerevisiae().head())
